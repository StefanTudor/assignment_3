
const FIRST_NAME = "Tudor";
const LAST_NAME = "Stefan";
const GRUPA = "1085";


class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails() {
        return this.name + " " + this.surname + " " + this.salary;
    }
}

class SoftwareEngineer extends Employee{
    constructor(name, surname, salary, experience) {
        super(name, surname, salary);
        if(experience == undefined) {
           this.experience = 'JUNIOR';
        } else {
            this.experience = experience;
        }
    }

    applyBonus() {
        switch(this.experience) {
            case 'JUNIOR' :
                return parseInt(this.salary * 1.1);
                break;
            case 'MIDDLE':
                return parseInt(this.salary * 1.15);
                break;
            case 'SENIOR':
                return parseInt(this.salary * 1.2);
                break;
            default:
                return parseInt(this.salary * 1.1);
        }
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

